#include <gfxfont.h>
#include <Adafruit_SPITFT.h>
#include <Adafruit_GrayOLED.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SPITFT_Macros.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <Wire.h>
#include <SPI.h>

#define RST 12
#define CE  13
#define DC  11
#define DIN  10
#define CLK  9

#define UP    0L 
#define LEFT  1L 
#define DOWN  2L 
#define RIGHT 3L 
#define NONE  4L

// Arduino digital pins associated with buttons
const byte PIN_BUTTON_A = 2; 
const byte PIN_BUTTON_B = 3;
const byte PIN_BUTTON_C = 4;
const byte PIN_BUTTON_D = 5;
const byte PIN_BUTTON_E = 6;
const byte PIN_BUTTON_F = 7;

// Arduino analog pins associated with joystick
const byte PIN_ANALOG_X = 0;
const byte PIN_ANALOG_Y = 1;

Adafruit_PCD8544 display = Adafruit_PCD8544(CLK, DIN, DC, CE, RST);

int valX = 0;
int valY = 0;
int fx   = 12;
int fy   = 11;

String edo = String("123456789ABCDEF "); 
long   mx  = NONE;

void setup() {
  Serial.begin(9600);

  // Setup: display
  display.begin();
  display.setContrast(60);  // adjust display contrast
  display.clearDisplay();
  
  // Define text size and color
  display.setTextSize(1);
  display.setTextColor(WHITE);

  random_move_edo(1000);
  display_escenario();
  //String n = "123456789ABCDEF ";
  display_numeros(edo);
  display.display();
  delay(1000);

  // Setup: joystick shield
  pinMode(PIN_BUTTON_A, INPUT);  
  digitalWrite(PIN_BUTTON_A, HIGH);  
  
  pinMode(PIN_BUTTON_B, INPUT);  
  digitalWrite(PIN_BUTTON_B, HIGH);
 
  pinMode(PIN_BUTTON_C, INPUT);  
  digitalWrite(PIN_BUTTON_C, HIGH);
 
  pinMode(PIN_BUTTON_D, INPUT);  
  digitalWrite(PIN_BUTTON_D, HIGH);
 
  pinMode(PIN_BUTTON_E, INPUT);  
  digitalWrite(PIN_BUTTON_E, HIGH);
 
  pinMode(PIN_BUTTON_F, INPUT);  
  digitalWrite(PIN_BUTTON_F, HIGH);  
}

void loop() {

  //mx = 4L;
  
  // put your main code here, to run repeatedly:
  if (digitalRead(PIN_BUTTON_A)==LOW){
    mx = UP;
  } else if (digitalRead(PIN_BUTTON_B)==LOW){
    mx = RIGHT;
  } else if (digitalRead(PIN_BUTTON_C)==LOW){
    mx = DOWN;
  } else if (digitalRead(PIN_BUTTON_D)==LOW){
    mx = LEFT;
  } else mx = NONE;

  move_edo(mx);
  display_escenario();
  display_numeros(edo);
  display.display();
  delay(500);  

}

void display_numeros(String n) {
  int  x, y;
  int  c = 0;
  for(int j=0; j<4; j++){
    y = (j * fy) + j;
    for(int k = 0; k<4; k++){
       x = (k * fx) + k;    
       display.setCursor(x + 3, y + 2);
       display.println(n[c]);
       c++;
    }
  }  
}

void display_escenario() {
  int  x, y;
  for(int j=0; j<4; j++){
    x = (j * fx) + j;
    for(int k = 0; k<4; k++){
       display.fillRoundRect(x, y, fx, fy, 3, 2);
       y = (k * fy) + k;    
    }
  }    
}

int pos(String s, char ca) { 
   int c=0; 
   while(s[c]!=ca)c++; 
   return c;   
}   

void move_edo(long mov) { 
   int p = pos(edo,' '); 
   if(mov == UP){ 
      if(p!=12 && p!=13 && p!=14 && p!=15){ 
         edo[p]   = edo[p+4]; 
         edo[p+4] = ' '; 
      } 
   } 
   if(mov == DOWN) { 
      if(p!=0 && p!=1 && p!=2 && p!=3){ 
         edo[p]   = edo[p-4]; 
         edo[p-4] = ' '; 
      } 
   }   
   if(mov == LEFT) { 
      if(p!=3 && p!=7 && p!=11 && p!=15){ 
         edo[p]   = edo[p+1]; 
         edo[p+1] = ' '; 
      } 
   } 
   if(mov == RIGHT) {     
      if(p!=0 && p!=4 && p!=8 && p!=12){ 
         edo[p]   = edo[p-1]; 
         edo[p-1] = ' '; 
      } 
   } 
} 

void random_move_edo(int movs) { 
   long randNumber; 
   int i = 0; 
   while(i < movs){ 
      randNumber = random(0, 4); 
      move_edo(randNumber); 
      i++; 
   }   
}   
