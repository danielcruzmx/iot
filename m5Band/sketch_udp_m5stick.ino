#include <WiFi.h>
#include <WiFiUdp.h>
#include <M5StickC.h>

#define BUFFER 150

// WiFi network name and password:
const char * networkName = "name_access_point";
const char * networkPswd = "password";

const int udpPort = 1234;
boolean connected = false;
char packetBuffer[BUFFER]; 
char clearlcd[BUFFER+50];

WiFiUDP udp;

void setup(){
  Serial.begin(115200);
  M5.begin();
  M5.Axp.ScreenBreath(10);
  pinMode(M5_LED, OUTPUT);
  digitalWrite(M5_LED, LOW);
  alert();
  M5.Lcd.setRotation(3);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setTextSize(1);
  M5.Lcd.setCursor(5, 0);
  M5.Lcd.println("WIFI");
  for(int j=0; j < (BUFFER + 50); j++) clearlcd[j] = ' ';
  clearlcd[BUFFER+50-1] = '\0';
  connectToWiFi(networkName, networkPswd);
}

void loop(){
  
  if(connected){
    M5.Lcd.setCursor(45, 0);
    M5.Lcd.println("CONNECTED   ");
    int packetSize = udp.parsePacket();
    if(packetSize){
      Serial.print("Received packet of size ");
      Serial.println(packetSize);
      M5.Lcd.setCursor(5, 15);
      M5.Lcd.println(clearlcd);     
      int len = udp.read(packetBuffer,BUFFER);
      if(len > 0){
         packetBuffer[len] = '\0';
      }
      alert();
      Serial.println("Contents:");
      Serial.println(packetBuffer);
      M5.Lcd.setCursor(5, 15);
      M5.Lcd.println(packetBuffer);     
    }
  } else {
    M5.Lcd.setCursor(45, 0);
    M5.Lcd.println("DISCONNECTED");
  }
  delay(1000);
  for(int j=0; j < BUFFER; j++) packetBuffer[j] = '\0';
  if(digitalRead(M5_BUTTON_HOME) == LOW){
     while(digitalRead(M5_BUTTON_HOME) == LOW);
     M5.Lcd.setCursor(5, 15);
     M5.Lcd.println(clearlcd);     
  }
}

void alert(){
  for(int j=1; j<4; j++){
      digitalWrite(M5_LED, LOW);
      delay(300);
      digitalWrite(M5_LED, HIGH);
      delay(300);
  }
}

void connectToWiFi(const char * ssid, const char * pwd){
  Serial.println("Connecting to WiFi network: " + String(ssid));
  WiFi.disconnect(true);
  WiFi.onEvent(WiFiEvent);
  WiFi.begin(ssid, pwd);
  Serial.println("Waiting for WIFI connection...");
}

void WiFiEvent(WiFiEvent_t event){
    switch(event) {
      case SYSTEM_EVENT_STA_GOT_IP:
          Serial.print("WiFi connected! IP address: ");
          Serial.println(WiFi.localIP());  
          udp.begin(WiFi.localIP(),udpPort);
          connected = true;
          break;
      case SYSTEM_EVENT_STA_DISCONNECTED:
          Serial.println("WiFi lost connection");
          connected = false;
          break;
    }
}
