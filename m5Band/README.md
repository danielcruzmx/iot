# M5Band notificaciones del movil al M5StickC

*  Sketch para M5StickC

// WiFi network name and password:

const char * networkName = "name_access_point";   // MOVIL

const char * networkPswd = "password";

const int udpPort = 1234;

La salida del Skecth por el puerto serie da la IP asigrana al dispositivo y muestra las notificaciones (sketch_output.png)

* Movil Access Point

El movil se configura como access point (movil_access_point.png)

* App MacroDroid

Se descarga e instala la App MacroDroid

El disparador es la recepcion de cualquier notificacion, puede configurarse para notificaciones de una App especifica

La accion es el envio de un paquete UDP (movil_macrodroid_config.png)

-NOTA.- MacroDroid presenta fallos en el envio de notificaciones, a veces es necesario reiniciar el movil

