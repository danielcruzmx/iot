/*
   ARDUINO ENTIENDE ROILA 
   ELABORADO POR DANIEL CRUZ CHAVEZ - JUNIO 2012
*/

#include < avr/pgmspace.h >
#define TOTAL_BUFF    142
#define TURN          98 
#define LIGHT         116
#define WAIT          102
#define VERDE         8
#define ROJO          9
#define AMARILLO      10
#define SPKR          11
#define ALL           11
#define STATUS        11
#define ESTADOS       8
#define SIMBOLOS      4
#define TAM_TOKEN     8
#define LEXEMAS       9
#define PRODUCCIONES  6
#define FACTOR_PROD   4
#define MAX_PILA      16
#define SENTENCIAS    10
#define BUFFER_ENT    10
#define BUFFER_ERR    40

/*
 El vocabulario Roila tiene tres tipos de palabras. 
 Nos referimos a ellas como las palabras de tipo CV, donde C puede
 ser cualquiera de las 11 consonantes y V cualquiera de las 5 vocales. 
 Este tipo de palabras demuestra empi­ricamente un mejor desempeño en 
 el reconocimiento de voz con respecto a las palabras de tipo no-CV. 
*/

prog_uchar vocalesRoila[]     PROGMEM  = {"AaEeIiOoUu"};
prog_uchar consonantesRoila[] PROGMEM  = {"BbFfJjKkLlMmNnPpSsTtWw"};

/*
 Estos son los tres tipos de palabras:
 CVCV
 CVCVC
 CVCVCV
   
 AFD para reconocimiento de palabras ROILA (S separador entre palabras)
   
        C       V       C       V       S
   (0)---->(1)---->(2)---->(3)---->(4)---->(7)
                                    |
                                  C |   S
                                   (5)---->(7)
                                    |
                                  V |   S
                                   (6)---->(7) 
*/                                   

PROGMEM prog_uint16_t T_EDOS[] = { // C  V  S  O   
                                      1,-1,-1,-1,  // edo 0
                                     -1, 2,-1,-1,  // edo 1
                                      3,-1,-1,-1,  // edo 2
                                     -1, 4,-1,-1,  // edo 3
                                      5,-1, 7,-1,  // edo 4
                                     -1, 6, 7,-1,  // edo 5
                                     -1,-1, 7,-1,  // edo 6
                                      7, 7, 7, 7   // edo 7
                                 };

/* 
   ARDUINO debe ser capaz de entender las instrucciones en ROILA:             
   ROILA
   botama [bawot|pusif] [koleti|tifeke|wipoba] foteli
   botama [bawot|pusif] foteli tuji
   fepasa     
    
   EQUIVALENTE EN INGLES
   turn [on|off] [green|red|yellow] light
   turn [on|off] lights
   wait
   EQUIVALENTE EN ESPAÑOL
   [encender|apagar] la luz [verde|roja|amarilla]    
   [encender|apagar] todas las luces
   hacer pausa                     
   
   EQUIVALENTES EN ARDUINO
   digitalWrite [HIGH | LOW] [PIN8 | PIN9 | PIN10] 
   digitalWrite [HIGH | LOW] PINES 8,9,10
   delay();
*/   
                      // ROILA         INGLES   ACCIONES
prog_char b[] PROGMEM = "botama ";  // Turn     digitalWrite(Y,Z);
prog_char t[] PROGMEM = "foteli ";  // Light         
prog_char f[] PROGMEM = "fepasa ";  // Wait     delay(X);
prog_char w[] PROGMEM = "bawot ";   // On       HIGH
prog_char p[] PROGMEM = "pusif ";   // Off      LOW
prog_char k[] PROGMEM = "koleti ";  // Green    PIN8
prog_char l[] PROGMEM = "tifeke ";  // Red      PIN9
prog_char m[] PROGMEM = "wipoba ";  // Yellow   PIN10
prog_char n[] PROGMEM = "tuji ";    // Very     PINES 8,9,10

PROGMEM const char *lexemas[] = {b,t,f,w,p,k,l,m,n};

/*
  ARDUINO debe entender la GRAMATICA:     
GRAMATICA  EN ROILA                  EN INGLES       
P-> A|B    P-> A|B                   P-> A|B       
A-> bCE    A-> botama C E            A-> turn C E    
B-> f      B-> fepasa                B-> wait           
C-> w|p    C-> bawot|pusif           C-> on|off           
D-> k|l|m  D-> koleti|tifeke|wipoba  D-> green|red| yellow
E-> Dt|tn  E-> D foteli|foteli tuji  E-> D light|lights
    ACCIONES EN ARDUINO
    P-> A | B
    A-> digitalWrite(E,C)
    B-> delay(1000)
    C-> HIGH | LOW
    D-> pin 8 | pin 9 | pin 10
    E-> D | pines 8,9,10
*/

prog_uchar   TERM[] PROGMEM={"btfwpklmn"};// SIMBOLOS TERMINALES DE LA GRAMATICA 
prog_uchar noTERM[] PROGMEM={"PABCDE"};// SIMBOLOS NO TERMINALES DE LA GRAMATICA 

// TABLA ANALISIS SINTACTICO 
//                      b   t   f   w   p   k   l   m   n   
prog_char P[] PROGMEM={"A       B                          "};//P->A|B 
prog_char A[] PROGMEM={"bCE                                "};//A->bCE
prog_char B[] PROGMEM={"        f                          "};//B->f
prog_char C[] PROGMEM={"            w   p                  "};//C->w|p
prog_char D[] PROGMEM={"                    k   l   m      "};//D->k|l|m
prog_char E[] PROGMEM={"    tn              Dt  Dt  Dt     "};//E->Dt|tn

PROGMEM const char *producciones[] = {P,A,B,C,D,E};
                        
char pila[MAX_PILA];
int  top=-1;
char entradas[SENTENCIAS][BUFFER_ENT];
int  parametros[SENTENCIAS][BUFFER_ENT];
int  acciones[LEXEMAS]={TURN,LIGHT,WAIT,HIGH,LOW,VERDE,ROJO,AMARILLO,ALL};

prog_char e1[] PROGMEM  = {"Una palabra es demasiado larga"}; 
prog_char e2[] PROGMEM  = {"Una palabra es invalida"}; 
prog_char e3[] PROGMEM  = {"Una palabra no es clave"}; 
prog_char e4[] PROGMEM  = {"Una palabra tiene simbolos incorrectos"}; 
prog_char e5[] PROGMEM  = {"Error de construccion de la instruccion"}; 
prog_char e6[] PROGMEM  = {"Desbordamiento de pila"}; 

PROGMEM const char *errores[] = {e1,e2,e3,e4,e5,e6};

int     tamCadena = 0;
char    buffer[TOTAL_BUFF];
boolean stringComplete = false;

void setup()
{
  Serial.begin(9600);
  pinMode(VERDE   , OUTPUT);     
  pinMode(ROJO    , OUTPUT);     
  pinMode(AMARILLO, OUTPUT);
  pinMode(STATUS  , OUTPUT);
  pinMode(SPKR    , OUTPUT);
  beep(1); // ready
}

void loop()
{
  if(stringComplete){
    interpreta(buffer, tamCadena);    
    stringComplete = false;
    beep(1); // ready
    tamCadena = 0;
  }
  serialEvent();
}

void serialEvent()
{
  while (Serial.available() && tamCadena < TOTAL_BUFF) {
    char inChar = (char)Serial.read(); 
    if (inChar == '~') buffer[tamCadena] = ' ';
    else buffer[tamCadena] = inChar;
    tamCadena++;
    if (inChar == '~') {
      // TERMINO EL TEXTO 
      stringComplete = true;
    } 
  }
}

void interpreta(char *buffer, int tamCadena)
{
  int stm,s;    
  stm = analizadorLexico(buffer, tamCadena);
  if(stm > 0){
    for(s=0;s < stm;s++){
       if(analizadorSintactico(s)){
           switch(parametros[s][0]){
             case TURN: 
               if(parametros[s][3]==ALL){
                 ledOnOff(VERDE,parametros[s][1]);
                 ledOnOff(ROJO,parametros[s][1]);
                 ledOnOff(AMARILLO,parametros[s][1]);
               } else ledOnOff(parametros[s][2],parametros[s][1]);
               break;
             case WAIT: 
               delay(1000);                
               break;
             default:
               break;              
           };    
       } else { beep(2); break; } //error(-5); sintactico         
    }   
  } else beep(1); //error(stm); lexico
}  

void ledOnOff(int puerto, int modo)
{
  digitalWrite(puerto, modo);  
}

int analizadorLexico(char *buffer, int tamCadena)
{
  int  p,x,y,n,s;
  char caracter;
  char token[TAM_TOKEN];
  int  ret = 0;
  char terminal;
  x=0;
  y=0;
  s=0;
  for(p=0;p < tamCadena;p++){   // RECORRE TODA LA CADENA
    caracter = simboloROILA(buffer[p]);//PERTENECE AL ALFABETO ROILA?
    switch (caracter) {                     
      case 'C':
        // ES UNA CONSONANTE VALIDA
        token[x] = buffer[p];
        x++;
        if(x  > (TAM_TOKEN - 2)) ret=-1;
        break;
      case 'V':
        // ES UNA VOCAL VALIDA Y LA GUARDA EN TOKEN  
        token[x] = buffer[p];
        x++;
        if(x > (TAM_TOKEN - 2)) ret=-1;
        break;
      case 'T':
        // ES UN TERMINADOR DE SENTENCIA
        if(x>0){
          token[x] = ' ';
          x++;
          token[x] = '\0';
          if(esToken(token,x)){ // DEL TIPO CVCV, CVCVC O CVCVCV
            n=numLexema(token,x); // QUE EL TOKEN SEA UNA PALABRA CLAVE
            if(n>=0){
              terminal = pgm_read_byte_near(TERM + n);
              entradas[s][y] = terminal;
              parametros[s][y] = acciones[n]; // PARAMETRIZA LA ACCION 
            } else ret = n;
          } else ret = -2;   // NO ES UNA PALABRA ROILA
          y++;
        }
        entradas[s][y] = '$';
        x=0;
        y=0;
        s++;
        break;
      case 'S':
        // ES UN SEPARADOR DE PALABRAS ROILA 
        if(x>0){ 
          token[x] = buffer[p];           
          x++;
          token[x] = '\0';
          if(esToken(token,x)){// DEL TIPO CVCV, CVCVC O CVCVCV
            n=numLexema(token,x);  // QUE EL TOKEN SEA UNA PALABRA CLAVE
            if(n>=0){
              terminal = pgm_read_byte_near(TERM + n);
              entradas[s][y] = terminal;
              parametros[s][y] = acciones[n]; // PARAMETRIZA LA ACCION 
            } else ret = n;
          } else ret = -2;   // NO ES UNA PALABRA ROILA
          x=0;
          y++;
        }
        break;  
      default: 
        // ERROR CARACTERES INCORRECTOS
        ret = -4;    // EL CARACTER NO ES PARTE DEL ALFABETO ROILA
        break;       // NI SEPARADOR NI TERMINADOR
    }
    if(ret != 0)break;
  }
  if(ret == 0) ret = s;
  return ret;
}

int numLexema(char *palabra, int tam)
{
  int i,j,c;
  char letra, caracter;
  int ret = -3;
  for (int i = 0; i < LEXEMAS; i++){
     c=0;
     for (j=0;j < tam;j++){
        letra = caracterLexema(i,j);
        caracter = palabra[j];
        // CONVIERTE A MINUSCULA
        if(caracter >= 65 and caracter <= 90)caracter |= ('a' - 'A');   
        if(caracter == letra)c++; 
     }
     if(c==tam){
       ret = i;
       break;
     }
  }  
  return ret;
}  

char caracterLexema(int i, int j)
{
  char buffer[TAM_TOKEN];
  strcpy_P(buffer, (char*)pgm_read_word(&(lexemas[i]))); 
  return buffer[j];
}  

boolean esToken(char *palabra, int tam)
{
  int i,j,ea;
  boolean ret = true;
  char caracter;
  char simbolo[5] = {'C','V','S','O'};
  ea=0;
  for(i=0;i < tam;i++){
    caracter = simboloROILA(palabra[i]);
    for(j=0;j<4;j++){
      if(caracter==simbolo[j]){
         ea = AFD(ea,j);
         if(ea==-1){
           ret=false;
         }
      } 
    }
  }
  return ret;
}

char simboloROILA(char caracter)
{
  char letra; 
  int  p;
  char S[4]  = {' ','\t','\r'};  // CARACTERES SEPARADORES
  char ret   = 'O';
  for(p=0;p<22;p++){
     if(p<10){
        letra =  pgm_read_byte_near(vocalesRoila + p); 
        if(letra == caracter) ret = 'V';
     }
     if(p<3)if(S[p]==caracter) ret = 'S'; 
     letra =  pgm_read_byte_near(consonantesRoila + p); 
     if(letra == caracter) ret = 'C';
  }
  if(caracter==',')ret = 'T'; // CARACTER TERMINAL DE SENTENCIA
  return ret;
}

int AFD(int estado, int simbolo)
{
  int p = estado * SIMBOLOS + simbolo;
  return pgm_read_word_near(T_EDOS + p);
}

boolean analizadorSintactico(int sentencia)
{
  int j, ae;
  char X, a;
  String prop;
  boolean ret = true;
  top = -1;              // INICIALIZA LA PILA 
  ae = 0;                // INICIALIZA APUNTADOR A LA ENTRADA
  if(entradas[sentencia][ae] != '$') {
    push('$');           // TERMINADOR
    push('P');           // PRODUCCION INICIAL
    do {
       X = pila[top];                // PRIMER ELEMENTO DE LA PILA
       a = entradas[sentencia][ae];  // APUNTADOR A LA ENTRADA
       if((esTERM(X) != -1) || (X=='$')){ // X ES TERMINAL O FIN PILA
         if(X == a){    
           pop();       // RETIRA ELEMENTO DE LA PILA 
           ae++;        // AVANZA APUNTADOR DE ENTRADA
         } else {
           ret = false;
           break;
         }
       } else {
         prop = esProduccion(X,a);  // X ES UNA PRODUCCION
         if(prop != ""){
            pop();           // RETIRA ELEMENTO DE LA PILA
            pushProp(prop);  // METE A LA PILA LA PROPOSICION 
         } else {
           ret = false;
           break;
         }
       }
    } while (X != '$');
  }
  return ret;
}

String esProduccion(char X, char a)
{
  char buffer[LEXEMAS * FACTOR_PROD];
  char prop[FACTOR_PROD];
  int  i,j,k,t;
  char simbolo;
  for(i=0;i < PRODUCCIONES;i++){
     simbolo = pgm_read_byte_near(noTERM + i);
     if(simbolo==X) break;
  }
  for(j=0;j < LEXEMAS;j++){
     simbolo = pgm_read_byte_near(TERM + j);
     if(simbolo==a) break;      
  }
  strcpy_P(buffer, (char*)pgm_read_word(&(producciones[i]))); 
  k = j * FACTOR_PROD;
  t = k + (FACTOR_PROD-1);
  i=0;
  while(k < t){
    if(buffer[k]!=' '){
       prop[i] = buffer[k];
       i++;
    }
    k++;
  }  
  prop[i]= '\0';
  return prop;
}

int esTERM(char caracter)
{
  int k=-1;
  char simbolo;
  for(int j=0;j < LEXEMAS;j++){
     simbolo = pgm_read_byte_near(TERM + j);
     if(simbolo==caracter){
        k=j;
        break;
     }  
  }
  return k;
}

void pushProp(String prop)
{
  int j,tam;
  char charBuf[10];
  prop.toCharArray(charBuf, 10); 
  tam = prop.length();
  for(j=tam-1;j>=0;j--){
    push(charBuf[j]);
  }
}

void push(char dato) 
{ 
  if(top >= MAX_PILA -1) beep(3);  //error(-6);
  else {
     top++; 
     pila[top] = dato; 
  }
}

void pop()
{
  if(top < 0)
     beep(3); //error(-6);
  else top--; 
}

void error(int error)
{
  char buffer[BUFFER_ERR];
  int i=0;
  error = error * (-1) - 1;
  strcpy_P(buffer, (char*)pgm_read_word(&(errores[error]))); 
  //Serial.println(buffer);
  for(i=0;i < error;i++){
    ledOnOff(STATUS, HIGH);
    delay(500);
    ledOnOff(STATUS, LOW);
  }  
}

void beep(int beeps)
{
  int i;
  for(i=0;i < beeps;i++){
    digitalWrite(SPKR, HIGH); 
    delay(300);               
    digitalWrite(SPKR, LOW);
    delay(300);     
  }
}