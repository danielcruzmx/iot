#include <M5StickC.h>

#define UP    0L 
#define LEFT  1L 
#define DOWN  2L 
#define AUTO  3L 
#define RIGHT 4L 

String edo = String("12345678 "); 
String sol = String("12345678 "); 

long mx, my;

boolean isEnd = false;
boolean ini   = false;

int16_t accX = 0;
int16_t accY = 0;
int16_t accZ = 0;

int ajx = -3;
int ajy = -10;

void setup() {
   M5.begin();
   M5.IMU.Init();
   M5.Lcd.setRotation(3);
   M5.Lcd.fillScreen(BLACK);
   M5.Lcd.setTextSize(2);
   M5.Lcd.setCursor(25, 0);
   M5.Lcd.println("M5 START");
   show_puzzle();
   ini = true;
}

void loop() {

   if(ini) {
      while (1) {
          if(digitalRead(M5_BUTTON_HOME) == LOW){
              while(digitalRead(M5_BUTTON_HOME) == LOW);
              break;
          }
      }
      ini = false;
      isEnd = false;
      M5.Lcd.setCursor(25, 0);
      M5.Lcd.println(" PUZZLE ");
      random_move();
      show_puzzle();
   }
   
   if(!isEnd){
    
      M5.IMU.getAccelAdc(&accX,&accY,&accZ);

      int accel_ang_x = (atan(accX/sqrt(pow(accY,2) + pow(accZ,2)))*(180.0/3.14)) + ajx ;
      int accel_ang_y = (atan(accY/sqrt(pow(accX,2) + pow(accZ,2)))*(180.0/3.14)) + ajy ;

      if(accel_ang_x > 1){
        mx = UP;      
      }
      if(accel_ang_x < -1){
        mx = DOWN;
      }
      if(accel_ang_y < -1){
        mx = RIGHT;
      }
      if(accel_ang_y > 1){
        mx = LEFT;
      }

      moves(mx);
      show_puzzle();

      delay(1000);
      
      if(edo == sol){ 
        M5.Lcd.setCursor(25, 0);
        M5.Lcd.println("M5 START");
        isEnd = true; 
        ini = true;
      } 
  }
}

void show_puzzle() { 
   M5.Lcd.setCursor(45, 20); M5.Lcd.println(edo[0]);
   M5.Lcd.setCursor(65, 20); M5.Lcd.println(edo[1]);
   M5.Lcd.setCursor(85, 20); M5.Lcd.println(edo[2]);
   M5.Lcd.setCursor(45, 40); M5.Lcd.println(edo[3]);
   M5.Lcd.setCursor(65, 40); M5.Lcd.println(edo[4]);
   M5.Lcd.setCursor(85, 40); M5.Lcd.println(edo[5]);
   M5.Lcd.setCursor(45, 60); M5.Lcd.println(edo[6]);
   M5.Lcd.setCursor(65, 60); M5.Lcd.println(edo[7]);
   M5.Lcd.setCursor(85, 60); M5.Lcd.println(edo[8]);
}

int pos(String s, char ca) { 
   int c=0; 
   while(s[c]!=ca)c++; 
   return c;   
}   

void moves(long mov) { 
   
   int p = pos(edo,' '); 
   
   if(mov == UP){ 
      if(p!=6 && p!=7 && p!=8){ 
         edo[p]   = edo[p+3]; 
         edo[p+3] = ' '; 
      } 
   } 
   
   if(mov == DOWN) { 
      if(p!=0 && p!=1 && p!=2){ 
         edo[p]   = edo[p-3]; 
         edo[p-3] = ' '; 
      } 
   }   
   
   if(mov == LEFT) { 
      if(p!=2 && p!=5 && p!=8){ 
         edo[p]   = edo[p+1]; 
         edo[p+1] = ' '; 
      } 
   } 
   
   if(mov == RIGHT) {     
      if(p!=0 && p!=3 && p!=6){ 
         edo[p]   = edo[p-1]; 
         edo[p-1] = ' '; 
      } 
   } 
} 

void random_move() { 
   long randNumber; 
   int i = 0; 
   while(i<500){ 
      randNumber = random(0, 5); 
      moves(randNumber); 
      i++; 
   }   
}   
