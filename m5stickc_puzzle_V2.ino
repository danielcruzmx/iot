#include <M5StickC.h>

#define UP    0L 
#define LEFT  1L 
#define DOWN  2L 
#define RIGHT 3L 

String edo    = String("123456789ABCDEF "); 

String sol[6] = {
                String("123456789ABCDEF "),
                String("159D26AE37BF48C "),
                String("7BE 48CF259D136A"),
                String("1234CDE5B F6A987"),
                String("789A612B543C FED"),
                String("FEDCBA987654321 ")
                }; 
String names[6] = {
                String(" Horizontal"),
                String("  Vertical "),
                String("  Diagonal "),                                                                
                String(" Periferico"), 
                String("  Espiral  "),
                String(" Imposible ")                                                                                                                              
                };

long    mx;
boolean isEnd, ini;
int16_t accX, accY, accZ;
int     ajx, ajy, em;

void setup() {
   isEnd = false;
   ini   = true;
   ajx   = -3;
   ajy   = -10;
   em    = 0;
   accX  = 0;
   accY  = 0;
   accZ  = 0;
   mx    = 0;
   M5.begin();
   M5.IMU.Init();
   pinMode(M5_LED, OUTPUT);  
   digitalWrite(M5_LED, LOW);  
   alert();
   M5.Axp.ScreenBreath(8);
   M5.Lcd.setRotation(0);
   M5.Lcd.fillScreen(BLACK);
   show_puzzle(sol[em]);
   show_messages("M5 Run", names[em]);
}

void loop() {

   if(ini) {
      // wait to start
      while (1) {
          if(digitalRead(M5_BUTTON_HOME) == LOW){
              // press M5 to start
              while(digitalRead(M5_BUTTON_HOME) == LOW);
              break;
          }
          if(digitalRead(M5_BUTTON_RST) == LOW){
            // press RST to select meta
            em = em + 1;
            if(em == 6) em = 0;
            show_puzzle(sol[em]);
            show_messages("M5 Run", names[em]);
            delay(1000);
          }
      }
      // start
      ini = false;
      isEnd = false;
      random_move_edo(500);
      show_puzzle(edo);
      show_messages(" PLAY ", names[em]);
   }

   if(digitalRead(M5_BUTTON_RST) == LOW){
      // press RST to show meta
      show_puzzle(sol[em]);
      show_messages(" META ", names[em]);
      
   } else {
     // play
     if(!isEnd){
          // not end, then play
          M5.IMU.getAccelAdc(&accX,&accY,&accZ);
          int accel_ang_x = (atan(accX/sqrt(pow(accY,2) + pow(accZ,2)))*(180.0/3.14)) + ajx ;
          int accel_ang_y = (atan(accY/sqrt(pow(accX,2) + pow(accZ,2)))*(180.0/3.14)) + ajy ;
          
          if(accel_ang_y < -1){
              mx = UP;
          }
          if(accel_ang_y > 1){
            mx = DOWN;      
          }
          if(accel_ang_x < -1){
            mx = RIGHT;
          } 
          if(accel_ang_x > 1){
            mx = LEFT;
          }

          move_edo(mx);
          show_puzzle(edo);
          show_messages(" PLAY ", names[em]);

          delay(1000);
      
          if(edo == sol[em]){ 
            // solved, finish 
            alert();
            show_messages("M5 Run", names[em]);
            isEnd = true; 
            ini = true;
          } 
     }
   }
}

void show_messages(String msg, String name) {
   M5.Lcd.setTextSize(2);
   M5.Lcd.setCursor(5, 95);
   M5.Lcd.println(msg);
   M5.Lcd.setTextSize(1);
   M5.Lcd.setCursor(5, 125);
   M5.Lcd.println(name);
}

void show_puzzle(String s) { 
   int f  = 20;
   int x = 5;
   int y = -20;
   M5.Lcd.setTextSize(2);
   for(int i=0 ; i<16; i++){
      if(!(i%4)) {
        x = 5; 
        y = y + f;
      }
      M5.Lcd.setCursor(x,y); M5.Lcd.println(s[i]);
      x = x + f;
   }
}

int pos(String s, char ca) { 
   int c=0; 
   while(s[c]!=ca)c++; 
   return c;   
}   

void move_edo(long mov) { 
   int p = pos(edo,' '); 
   if(mov == UP){ 
      if(p!=12 && p!=13 && p!=14 && p!=15){ 
         edo[p]   = edo[p+4]; 
         edo[p+4] = ' '; 
      } 
   } 
   if(mov == DOWN) { 
      if(p!=0 && p!=1 && p!=2 && p!=3){ 
         edo[p]   = edo[p-4]; 
         edo[p-4] = ' '; 
      } 
   }   
   if(mov == LEFT) { 
      if(p!=3 && p!=7 && p!=11 && p!=15){ 
         edo[p]   = edo[p+1]; 
         edo[p+1] = ' '; 
      } 
   } 
   if(mov == RIGHT) {     
      if(p!=0 && p!=4 && p!=8 && p!=12){ 
         edo[p]   = edo[p-1]; 
         edo[p-1] = ' '; 
      } 
   } 
} 

void random_move_edo(int movs) { 
   long randNumber; 
   int i = 0; 
   while(i < movs){ 
      randNumber = random(0, 4); 
      move_edo(randNumber); 
      i++; 
   }   
}   

void alert() {  
   for(int j=1; j<4; j++){      
      digitalWrite(M5_LED, LOW);      
      delay(300);      
      digitalWrite(M5_LED, HIGH);      
      delay(300);  
   }
}